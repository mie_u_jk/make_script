# hostsへ追加
sudo sh -c 'echo 127.0.1.1 $(hostname) >> /etc/hosts'

# リポジトリサーバの応答が100KB/秒以下と遅い場合に以下の3つの#をとって実行(GUI設定は全てリターンでOK)
#sudo add-apt-repository ppa:saiarcot895/myppa
#sudo apt-get update
#sudo apt-get -y install apt-fast


# 日本のリポジトリへ変更
sudo sed -i -e 's%http://.*.ubuntu.com%http://ftp.jaist.ac.jp/pub/Linux%g' /etc/apt/sources.list


sudo apt -y update
sudo apt -y upgrade
sudo apt -y upgrade

# インストール
sudo apt-add-repository ppa:webupd8team/java
sudo apt-add-repository ppa:texlive-backports/ppa
sudo apt -y update
sudo apt -y -f install oracle-java8-installer make automake gcc
sudo apt -y -f install oracle-java8-installer make automake gcc

# EzGraphインストール
git clone http://gitlab.arch.info.mie-u.ac.jp/EzGraph/EzGraph.git
cd EzGraph
./configure --prefix=/usr
sudo make && sudo make install

sudo apt -y -f install texlive-lang-cjk eclipse-platform xterm lxterminal gnome-terminal xfce4-terminal vim git uim uim-fep uim-anthy libjpeg-dev libgif-dev libtiff5-dev libncurses5-dev libgtk-3-dev libgnutls-dev libmagick++-dev x11-apps software-properties-common gnuplot fonts-ipafont python ruby evince tgif unifont fonts-ipafont libxpm-dev emacs anthy-el firefox
sudo apt -y -f install texlive-lang-cjk eclipse-platform xterm lxterminal gnome-terminal xfce4-terminal vim git uim uim-fep uim-anthy libjpeg-dev libgif-dev libtiff5-dev libncurses5-dev libgtk-3-dev libgnutls-dev libmagick++-dev x11-apps software-properties-common gnuplot fonts-ipafont python ruby evince tgif unifont fonts-ipafont libxpm-dev emacs anthy-el firefox


# .bashrcへの設定追加
cat << EOS >> ~/.bashrc
export XMODIFIERS=@im=uim
export DISPLAY=localhost:0.0
export LANG=ja_JP.UTF-8
export LC_MESSAGES=ja_JP.UTF-8
export LC_IDENTIFICATION=ja_JP.UTF-8
export LC_COLLATE=ja_JP.UTF-8
export LC_MEASUREMENT=ja_JP.UTF-8
export LC_CTYPE=ja_JP.UTF-8
export LC_TIME=ja_JP.UTF-8
export LC_NAME=ja_JP.UTF-8
export DISPLAY=localhost:0.0
export XIM=uim
export XMODIFIERS=@im=uim
export UIM_CANDWIN_PROG=uim-candwin-gtk
#export UIM_CANDWIN_PROG=uim-candwin-qt
export GTK_IM_MODULE=uim
export QT_IM_MODULE=uim
alias term='xfce4-terminal'
if [ \$SHLVL -eq 1 ]; then
uim-xim &
fi
EOS

# .uimへの設定追加（X利用時の日本語入力切替はCtrl+スペース、X未使用時は、半角/全角キー）
cat << EOS >> ~/.uim
(define default-im-name 'anthy)
(define-key generic-on-key? '("<Control> " "\`"))
(define-key generic-off-key? '("<Control> " "\`"))
EOS

# .emacs.d/init.elへの設定追加
mkdir ~/.emacs.d
cat << EOS > ~/.emacs.d/init.el
;;; 日本語をデフォルトにする。
(set-language-environment "Japanese")
;;; anthy.elをロードできるようにする（必要に応じて）。
(push "/usr/share/emacs/site-lisp/anthy/" load-path)
;;; anthy.elをロードする
(load-library "anthy")
(load-file "/usr/share/emacs/site-lisp/anthy/leim-list.el")
;;; japanese-anthyをディフォルトのinput-methodにする。
(setq default-input-method 'japanese-anthy)
EOS




# UIM-XIMのインストール
sudo apt -y -f install uim-xim locales
sudo apt -y -f install uim-xim locales

#sudo dpkg-reconfigure locales
sudo locale-gen ja_JP.UTF-8
sudo update-locale LANG=ja_JP.UTF-8




